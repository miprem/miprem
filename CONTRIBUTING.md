# Contributing to Miprem

Thank you for contributing to Miprem! <3

This is the repository for **Miprem templates**. If you want to work on a **Miprem implementation**, please follow instructions on their corresponding repositories:

- [Javascript implementation](https://framagit.org/roipoussiere/miprem-js);
- [Python implementation](https://framagit.org/roipoussiere/miprem-python);
- [PHP implementation](https://framagit.org/roipoussiere/miprem-php).

## Sending feedback

There are several places where you can report issues or suggest improvements:

- in the [GitLab repository](https://framagit.org/roipoussiere/miprem/-/issues);
- in the french [Mieux Voter forum](https://forum.mieuxvoter.fr/).

We are particularly open for discussion about image design and accessibility.

## Developping

Clone the Miprem repository:

```bash
git clone https://framagit.org/roipoussiere/miprem.git
```

## Documentation

Miprem documentation website is generated with [MkDocs](https://www.mkdocs.org/).

There are two ways to build the documentation:

### Use the docker image

```
docker run --rm -it -v ${PWD}:/docs squidfunk/mkdocs-material build
```

### Install Mkdocs locally

```bash
python -m venv .venv # create venv
source .venv/bin/activate # source venv
pip install mkdocs-material # install MkDocs and theme
mkdocs --version # check installation
```

Then build doc with `mkdocs build`.
