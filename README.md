# Miprem

_A **m**er**i**t **p**rofile **re**nderer for **m**ajority judgment._

This project aims to generate vector and bitmap images representing the merit profile of a
[majority judgment](https://en.wikipedia.org/wiki/Majority_judgment) election, based on the merit profile data.
Miprem does not process poll results, it just render as pretty as possible.

It's based on Handlebars template engine, implemented in various languages, so Miprem implementation in another language should be really easy and will render exact same images.

See it in action on the [demo page](https://miprem.frama.io/miprem-js)!

![](poll_samples/generated/movies.svg)

## Features

- implemented in Javascript, Python and PHP (other languages could be easily added);
- exported images have fixed dimensions, so you can embed them in places that have a specific aspect ratio;
- svg includes `<title>` attributes, that describes the content in tooltips, used by reading softwares ;

## Implementations

Miprem is implemented in several languages, that all render the exact same images:

- Javascript: [code](https://framagit.org/roipoussiere/miprem-js), [online demo](https://roipoussiere.frama.io/miprem-js);
- Python: [code](https://framagit.org/roipoussiere/miprem-python);
- PHP: [code](https://framagit.org/roipoussiere/miprem-php)

Miprem uses the [Handlebars minimal template engine](https://handlebarsjs.com/), which is implemented in various languages. Thus implementing Miprem in your other favorite language should be trivial - feel free to [contribute](./CONTRIBUTING.md).

## Input files

Miprem generates images based on 3 distinct resources:

- **poll**: json file that represent the raw results of an election;
- **config**: json file that lists options to configure Miprem;
- **style**: css file to change the style of rendered images.

## Contributing

Woohoo thanks! Please read the [contribution guide](./CONTRIBUTING.md).

## Licence & authorship

This project is developed by the [MieuxVoter non-profit organization](http://mieuxvoter.fr/) and published under [MIT licence](./LICENCE).
